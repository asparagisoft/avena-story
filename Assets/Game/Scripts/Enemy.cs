﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace Assets.Game.Scripts
{
    public class Enemy : MonoBehaviour
    {
        private EnemyData _enemyData;
        [SerializeField]
        private SpriteRenderer _sprite;

        private int _health;
        public void Init(EnemyData enemyData)
        {
            _enemyData = enemyData;

            _health = enemyData.Health;

            Debug.Log("Init Enemy");
            _sprite.color = new Color(255,255,255,0);
            _sprite.DOColor(new Color(255,255,255,1), 2f);
            //LeanTween.color(this.gameObject, Color.black, 2f);

        }
    }

   
}
