﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Game.Scripts.Battle;
using Fungus;
using MEC;
using MonsterLove.StateMachine;
using Slash.Unity.DataBind.Core.Presentation;
using UnityEngine;

namespace Assets.Game.Scripts
{
    public class BattleController : MonoBehaviour
    {
        [SerializeField]
        private List<EnemySlotInternal> _enemies;

        [SerializeField]
        private Flowchart _flowchart;

        private BattleContext _battleContext;

        private Enemy _enemy;

        [SerializeField]
        private ProjectileEmitter _emitter;

        private enum BattleStates
        {
            Intro,
            PlayerTurn,
            EnemyTurn,
            BattleOver
        }

        private StateMachine<BattleStates> _machine;



        private void Start()
        {
            _machine = StateMachine<BattleStates>.Initialize(this);
            _machine.ChangeState(BattleStates.Intro);
            //_battleContext = new BattleContext();

            //BindingSet<databin>
            var contextHolder = GetComponent<ContextHolder>();
            _battleContext = contextHolder.Context as BattleContext;
            _battleContext.PlayerName = "boh";
            _battleContext.Health = 100;
            _battleContext.PlayerMenuEnabled = false;
            _battleContext.StatusMenuEnabled = false;
        }



        public void BeginBattle(Action onComplete)
        {
            if (_machine.State == BattleStates.Intro)
            {
                StartCoroutine(StartBattle(onComplete));
            }
            //StartCoroutine(DoStuff());

            //Fungus.SayDialog.ActiveSayDialog.Say("spaghetti", true, true, false, true, false, null, null);
        }

        private IEnumerator StartBattle(Action onComplete)
        {
            yield return StartCoroutine(SpawnEnemies());

            _battleContext.StatusMenuEnabled = true;
            _machine.ChangeState(BattleStates.PlayerTurn);

            yield return new WaitUntil(() => _machine.State == BattleStates.BattleOver);

            //while (_machine.State != BattleStates.BattleOver)
            //{
            //    yield return Timing.waitf;
            //}
            this.gameObject.SetActive(false);

            onComplete();
        }

        public void EndBattle()
        {
            _machine.ChangeState(BattleStates.BattleOver);
        }

        public IEnumerator SpawnEnemies()
        {
            Debug.Log("Spawning enemies");
            foreach (var enemy in _enemies)
            {
                var spawnedEnemy = Instantiate(enemy.Data.EnemyPrefab, enemy.Slot.transform.position, Quaternion.identity);
                spawnedEnemy.transform.parent = this.transform;
                var enemyComponent = spawnedEnemy.GetComponent<Enemy>();
                _enemy = enemyComponent;
                enemyComponent.Init(enemy.Data);

                _battleContext.EnemyName = enemy.Data.EnemyName;
                _battleContext.EnemyHealth = enemy.Data.Health;
                _battleContext.EnemyPsyche = enemy.Data.Psyche;
            }



            yield return null;
        }

        private void PlayerTurn_Enter()
        {
            _battleContext.PlayerMenuEnabled = true;
        }

        private void PlayerTurn_Exit()
        {
            _battleContext.PlayerMenuEnabled = false;
        }

        private void EnemyTurn_Enter()
        {
            //_battleContext.PlayerMenuEnabled = false;
            _battleContext.PlayerDefendAreaActive = true;

            Timing.RunCoroutine(DoEnemyTurn());
        }

        private void EnemyTurn_Exit()
        {
            
            _battleContext.PlayerDefendAreaActive = false;
        }

        private IEnumerator<float> DoEnemyTurn()
        {
            _emitter.StartShooting();
            yield return Timing.WaitForSeconds(10f);
            _emitter.StopShooting();

            _machine.ChangeState(BattleStates.PlayerTurn);
        }

        public void Aggress()
        {
            _battleContext.PlayerMenuEnabled = false;
            StartCoroutine(DoAggression());
        }

        private IEnumerator DoAggression()
        {
            yield return StartCoroutine(Say($"Attacco: in corso, danni: {10}", null, false, true, true, true, false, null));
            _battleContext.EnemyHealth -= 10;

            if (_battleContext.EnemyHealth <= 0)
            {
                Destroy(_enemy.gameObject);
                yield break;
            }

            _machine.ChangeState(BattleStates.EnemyTurn);
        }

        private IEnumerator Say(string text, Character character, bool extendPrevious, bool waitForClick, bool fadeWhenDone, bool stopVoiceover, bool waitForVO, AudioClip voiceOverClip)
        {


            // Override the active say dialog if needed
            if (character != null && character.SetSayDialog != null)
            {
                SayDialog.ActiveSayDialog = character.SetSayDialog;
            }



            var sayDialog = SayDialog.GetSayDialog();
            if (sayDialog == null)
            {

                yield break;
            }

            var flowchart = _flowchart;

            sayDialog.SetActive(true);

            sayDialog.SetCharacter(character);
            //sayDialog.SetCharacterImage(portrait);

            string displayText = text;

            var activeCustomTags = CustomTag.activeCustomTags;
            for (int i = 0; i < activeCustomTags.Count; i++)
            {
                var ct = activeCustomTags[i];
                displayText = displayText.Replace(ct.TagStartSymbol, ct.ReplaceTagStartWith);
                if (ct.TagEndSymbol != "" && ct.ReplaceTagEndWith != "")
                {
                    displayText = displayText.Replace(ct.TagEndSymbol, ct.ReplaceTagEndWith);
                }
            }

            string subbedText = flowchart.SubstituteVariables(displayText);
            yield return StartCoroutine(sayDialog.DoSay(subbedText, !extendPrevious, waitForClick, fadeWhenDone, stopVoiceover,
                waitForVO, voiceOverClip, delegate { }));
            //sayDialog.Say(subbedText, !extendPrevious, waitForClick, fadeWhenDone, stopVoiceover, waitForVO, voiceOverClip, delegate {

            //});

        }

        public void Insult()
        {

        }

        public void Waifu()
        {

        }

        public void Items()
        {

        }

        //private IEnumerator DoStuff()
        //{
        //    //int i = 0;
        //    foreach (var enemy in _enemies)
        //    {

        //        //i++;
        //    }


        //    yield return _flowchart.FindBlock("Test").Execute();
        //}



    }

    [Serializable]
    public class EnemySlotInternal
    {
        public EnemyData Data;
        public GameObject Slot;

    }
}
