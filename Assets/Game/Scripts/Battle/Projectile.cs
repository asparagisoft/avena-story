﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEC;
using PathologicalGames;
using UnityEngine;

namespace Assets.Game.Scripts.Battle
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField]
        private float _speed = 2f;

        [SerializeField]
        private float _lifeSpan = 4f;

        [SerializeField]
        private int _damage = 5;

        public int Damage => _damage;

        private CoroutineHandle _timer;

        public void OnSpawned()
        {
            _timer = Timing.RunCoroutine(TimeDespawn());
        }

        private IEnumerator<float> TimeDespawn()
        {
            yield return Timing.WaitForSeconds(_lifeSpan);

            PoolManager.Pools["Projectiles"].Despawn(this.transform);
        }

        private void Update()
        {
            this.transform.Translate(Vector3.down * Time.deltaTime);
        }

        public void OnDespawned()
        {
            Timing.KillCoroutines(_timer);
        }
    }
}
