﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEC;
using PathologicalGames;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Game.Scripts.Battle
{
    public class ProjectileEmitter : MonoBehaviour
    {
        [SerializeField]
        private Projectile _projectilePrefab;

        [SerializeField]
        private float _interval = 1f;

        private bool _isShooting = false;

        [Button]
        public void StartShooting()
        {
            if (!_isShooting)
            {
                _isShooting = true;
                Timing.RunCoroutine(Shoot().CancelWith(this.gameObject));
            }


        }

        [Button]
        public void StopShooting()
        {
            if (_isShooting)
            {
                _isShooting = false;
            }
        }

        private IEnumerator<float> Shoot()
        {
            while (_isShooting)
            {
                yield return Timing.WaitForSeconds(_interval);
                PoolManager.Pools["Projectiles"].Spawn(_projectilePrefab.transform, this.transform.position, Quaternion.identity);
            }

        }

    }
}
