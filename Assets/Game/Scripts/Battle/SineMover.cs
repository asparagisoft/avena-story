﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts.Battle
{
    public class SineMover : MonoBehaviour
    {
        [SerializeField]
        private float _speed = 2f;

        [SerializeField]
        private float _frequency = 20f;

        [SerializeField]
        private float _magnitude = 0.5f;

        private Vector3 _axis;

        private Vector3 _startPosition;

        private void Start()
        {
            _startPosition = this.transform.position;
            _axis = Vector3.right;
        }

        private void Update()
        {
            //_startPosition += transform.up * Time.deltaTime * _speed;
            this.transform.position = _startPosition + _axis * Mathf.Sin(Time.time * _frequency) * _magnitude;

            //this.transform.position = _startPosition + new Vector3(Mathf.Sin(Time.time), 0, 0) * Time.deltaTime;

        }
    }
}
