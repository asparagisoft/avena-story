﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Game.Scripts.Battle;
using Sirenix.OdinInspector;
using Slash.Unity.DataBind.Core.Presentation;
using UnityEngine;

namespace Assets.Game.Scripts
{
    public class PlayerController : MonoBehaviour
    {

        [SerializeField]
        private Rect _boundingBoxDimensions;

        [SerializeField]
        private float _speed;

        [SerializeField]
        private int _units = 100;
        private Vector2 _origin;

        [SerializeField]
        private SpriteRenderer _backgroundTexture;

        [SerializeField]
        private LayerMask _damageMask;

        public Rect BoundingBoxDimensions => _boundingBoxDimensions;

        [SerializeField] private ContextHolder _contextHolder;

        private BattleContext _battleContext;

        private void Awake()
        {
            
            
            

            _origin = this.transform.position;

            UpdateBackground();
        }

        private void Start()
        {
            _battleContext = _contextHolder.Context as BattleContext;
        }

        [Button]
        private void UpdateBackground()
        {
            _backgroundTexture.transform.localScale = new Vector3(_boundingBoxDimensions.width, _boundingBoxDimensions.height, 1);
            _backgroundTexture.transform.localPosition = _boundingBoxDimensions.center;
        }

        public void Update()
        {
            var horizontal = Input.GetAxisRaw("Horizontal");
            var vertical = Input.GetAxisRaw("Vertical");

            float horizontalTranslation = 0F;
            float verticalTranslation = 0f;

            if (horizontal != 0)
            {
                var destination = this.transform.position.x + (horizontal * _speed * Time.deltaTime);

                Debug.DrawLine(this.transform.position, new Vector3(destination, this.transform.position.y), Color.blue);

                if (destination < _boundingBoxDimensions.xMax && destination > _boundingBoxDimensions.xMin)
                {
                    horizontalTranslation = horizontal * _speed * Time.deltaTime;

                }


            }

            if (vertical != 0)
            {
                var destination = this.transform.position.y + (vertical * _speed * Time.deltaTime);


                Debug.DrawLine(this.transform.position, new Vector3(this.transform.position.x, destination), Color.green);
                if (destination > _boundingBoxDimensions.yMin && destination < _boundingBoxDimensions.yMax)
                {
                    verticalTranslation = vertical * _speed * Time.deltaTime;

                }

            }

            this.transform.Translate(horizontalTranslation, verticalTranslation, 0);


            if (this.transform.position.x > _boundingBoxDimensions.xMax)
            {
                this.transform.position = new Vector3(_boundingBoxDimensions.xMax, this.transform.position.y);
            }
            if (this.transform.position.x < _boundingBoxDimensions.xMin)
            {
                this.transform.position = new Vector3(_boundingBoxDimensions.xMin, this.transform.position.y);
            }
            if (this.transform.position.y > _boundingBoxDimensions.yMax)
            {
                this.transform.position = new Vector3(this.transform.position.x, _boundingBoxDimensions.yMax);
            }
            if (this.transform.position.y < _boundingBoxDimensions.yMin)
            {
                this.transform.position = new Vector3(this.transform.position.x, _boundingBoxDimensions.yMin);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            var projectile = collision.gameObject.GetComponent<Projectile>();
            if (projectile)
            {
                _battleContext.Health -= projectile.Damage;
            }
        }




        //private void DebugDrawColoredRectangle(Rect dimensions, Color color)
        //{
        //    Debug.DrawLine( dimensions.position, new Vector3(dimensions.position.x + dimensions.width size, dimensions.position.y, 0), color);
        //    Debug.DrawLine(position, new Vector3(position.x, position.y – size, position.z), color);
        //    Debug.DrawLine(new Vector3(position.x, position.y – size, position.z), new Vector3(position.x + size, position.y – size, position.z), color);
        //    Debug.DrawLine(new Vector3(position.x + size, position.y – size, position.z), new Vector3(position.x + size, position.y, position.z), color);
        //}

    }



    //[CustomEditor(typeof(PlayerController))]
    //public class PlayerControllerEditor : Editor
    //{
    //    public override void OnInspectorGUI()
    //    {
    //        base.DrawDefaultInspector();
    //        var player = target as PlayerController;
    //        EditorGUI.DrawRect(player.BoundingBoxDimensions, Color.green);
    //    }
    //}
}
