﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game.Scripts
{
    [CreateAssetMenu(menuName = "Battle/EnemyData")]
    public class EnemyData : ScriptableObject
    {
        [SerializeField] public string EnemyName;
        [SerializeField] public int Health;
        [SerializeField] public int Psyche;
        [SerializeField] public int Defense;

        [SerializeField] public GameObject EnemyPrefab;

        
        public Insult InsultsList;
    }
}
