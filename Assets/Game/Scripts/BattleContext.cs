﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Slash.Unity.DataBind.Core.Data;

namespace Assets.Game.Scripts
{
    public class BattleContext : Context
    {
        private readonly Property<int> healthProperty = new Property<int>();
        private readonly Property<int> psycheProperty = new Property<int>();
        private readonly Property<int> memeProperty = new Property<int>();

        private readonly Property<int> enemyHealthProperty = new Property<int>();
        private readonly Property<int> enemyPsycheProperty = new Property<int>();

        private readonly Property<string> playerNameProperty = new Property<string>();
        private readonly Property<string> enemyNameProperty = new Property<string>();

        private readonly Property<bool> playerMenuEnabledProperty = new Property<bool>();

        private readonly Property<bool> statusMenuEnabledProperty = new Property<bool>();

        private readonly Property<bool> playerDefendAreaActiveProperty = new Property<bool>();

        public int Health
        {
            get => this.healthProperty.Value;
            set => this.healthProperty.Value = value;
        }

        public int Psyche
        {
            get => this.psycheProperty.Value;
            set => this.psycheProperty.Value = value;
        }

        public int EnemyHealth
        {
            get => this.enemyHealthProperty.Value;
            set => this.enemyHealthProperty.Value = value;
        }

        public int EnemyPsyche
        {
            get => this.enemyPsycheProperty.Value;
            set => this.enemyPsycheProperty.Value = value;
        }

        public string PlayerName
        {
            get => this.playerNameProperty.Value;
            set => this.playerNameProperty.Value = value;
        }

        public string EnemyName
        {
            get => this.enemyNameProperty.Value;
            set => this.enemyNameProperty.Value = value;
        }

        public bool PlayerMenuEnabled
        {
            get => this.playerMenuEnabledProperty.Value;
            set => this.playerMenuEnabledProperty.Value = value;
        }

        public bool StatusMenuEnabled
        {
            get => this.statusMenuEnabledProperty.Value;
            set => this.statusMenuEnabledProperty.Value = value;
        }

        public bool PlayerDefendAreaActive
        {
            get => this.playerDefendAreaActiveProperty.Value;
            set => this.playerDefendAreaActiveProperty.Value = value;
        }

        public int Meme
        {
            get => this.memeProperty.Value;
            set => this.memeProperty.Value = value;
        }

        public BattleContext()
        {

        }
    }
}
