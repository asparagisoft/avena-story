﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Game.Scripts
{
    [CreateAssetMenu(menuName = "Battle/Insults")]
    public class Insult : SerializedScriptableObject
    {
        public Dictionary<string, string> Insults;
    }
}
