﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Slash.Unity.DataBind.Foundation.Setters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Setters
{


    /// <summary>
    ///   Set the text of a Text depending on the string data value.
    /// </summary>
    [AddComponentMenu("Data Bind/UnityUI/Setters/[DB] Energy Bar Setter")]
    public class EnergyBarSetter : ComponentSingleSetter<EnergyBar, int>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(EnergyBar target, int value)
        {
            target.valueCurrent = value;
        }
    }
}
