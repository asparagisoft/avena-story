﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Slash.Unity.DataBind.Foundation.Setters;
using UnityEngine;

namespace Assets.Game.Setters
{
    public class ActiveSetterInvertible : GameObjectSingleSetter<bool>
    {
        /// <summary>
        ///     Indicates that setter doesn't get disabled when component is disabled.
        ///     This way the active setter will still check the data value even if the game object
        ///     the setter is on is disabled.
        /// </summary>
        [Tooltip(
            "Indicates that setter doesn't get disabled when component is disabled. This way the active setter will still check the data value even if the game object the setter is on is disabled.")]
        public bool DontDisableOnInactive;

        public bool Invert;
        /// <inheritdoc />
        public override void Disable()
        {
            if (!this.DontDisableOnInactive)
            {
                base.Disable();
            }
        }

        /// <inheritdoc />
        protected override void OnValueChanged(bool newValue)
        {
            if (!Invert)
            {
                this.Target.SetActive(newValue);
            }
            else
            {
                this.Target.SetActive(!newValue);
            }
        }
    }
}
