﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;

namespace Assets.Game.Setters
{
    using Slash.Unity.DataBind.Foundation.Setters;

    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    ///   Set the text of a Text depending on the string data value.
    /// </summary>
    [AddComponentMenu("Data Bind/UnityUI/Setters/[DB] TextMesh UGUI Text Setter (Unity)")]
    public class TextMeshUGUITextSetter : ComponentSingleSetter<TextMeshProUGUI, string>
    {
        /// <inheritdoc />
        protected override void UpdateTargetValue(TextMeshProUGUI target, string value)
        {
            target.text = value;
        }
    }
}
