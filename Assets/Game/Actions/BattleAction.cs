﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Game.Scripts;
using Fungus;
using UnityEngine;

namespace Assets.Game.Actions
{
    [CommandInfo("battle", "Start Battle", "Starts a battle")]
    [AddComponentMenu("")]
    public class BattleAction : Command
    {
        [SerializeField]
        protected BattleController _battleController;

        public override void OnEnter()
        {
            _battleController.BeginBattle(delegate
            {
                Destroy(_battleController.gameObject);
                Continue();
            });
        }
    }
}
