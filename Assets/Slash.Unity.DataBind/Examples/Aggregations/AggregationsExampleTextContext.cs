﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AggregationsExampleTextContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Examples.Aggregations
{
    using System;
    using Slash.Unity.DataBind.Core.Data;

    public class AggregationsExampleTextContext : Context
    {
        /// <inheritdoc />
        public AggregationsExampleTextContext(string text)
        {
            this.Text = text;
        }

        public event Action<AggregationsExampleTextContext> Remove;

        public string Text { get; private set; }

        public void DoRemove()
        {
            this.OnRemove();
        }

        private void OnRemove()
        {
            var handler = this.Remove;
            if (handler != null)
            {
                handler(this);
            }
        }
    }
}