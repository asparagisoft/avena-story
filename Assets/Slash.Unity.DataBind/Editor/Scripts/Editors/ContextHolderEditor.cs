﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextHolderEditor.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Editor.Editors
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Core.Utils;
    using Slash.Unity.DataBind.Editor.Utils;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    ///     Custom editor for <see cref="ContextHolder" />.
    /// </summary>
    [CustomEditor(typeof(ContextHolder))]
    public class ContextHolderEditor : Editor
    {
        private const int MaxLevel = 5;

        private readonly Dictionary<string, bool> foldoutDictionary =
            new Dictionary<string, bool>();

        private readonly Dictionary<DataDictionary, object> newKeys = new Dictionary<DataDictionary, object>();

        private Rect invokeEventPopupRect;

        /// <summary>
        ///     Unity callback.
        /// </summary>
        public override void OnInspectorGUI()
        {
            var contextHolder = this.target as ContextHolder;
            if (contextHolder == null)
            {
                return;
            }

            if (Application.isPlaying)
            {
                var context = contextHolder.Context;

                // Use data class if a wrapper is used.
                var notifyPropertyChangedDataContext = context as NotifyPropertyChangedDataContext;
                if (notifyPropertyChangedDataContext != null)
                {
                    context = notifyPropertyChangedDataContext.DataObject;
                }

                if (context != null)
                {
                    var contextType = context.GetType().ToString();

                    EditorGUILayout.LabelField("Context", contextType);

                    // Reflect data in context.
                    this.DrawContextData(context);

                    EditorUtility.SetDirty(contextHolder);
                }
                else
                {
                    if (contextHolder.ContextType != null && GUILayout.Button("Create context"))
                    {
                        contextHolder.Context = Activator.CreateInstance(contextHolder.ContextType);
                    }
                }
            }
            else
            {
                base.OnInspectorGUI();
            }
        }

        private void DrawContextData(object context)
        {
            if (context == null)
            {
                return;
            }

            this.DrawContextData(context, 1);
        }

        private void DrawContextData(object context, int level)
        {
            if (context == null)
            {
                return;
            }

            var prevIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = level;

            var contextMemberInfos = ContextTypeCache.GetMemberInfos(context.GetType());
            foreach (var contextMemberInfo in contextMemberInfos)
            {
                if (contextMemberInfo.Property != null)
                {
                    var memberValue = contextMemberInfo.Property.GetValue(context, null);
                    var newMemberValue = this.DrawContextData(contextMemberInfo.Name,
                        contextMemberInfo.Property.PropertyType, memberValue, level);
                    if (contextMemberInfo.Property.CanWrite && !Equals(newMemberValue, memberValue))
                    {
                        contextMemberInfo.Property.SetValue(context, newMemberValue, null);
                    }
                }
                else if (contextMemberInfo.Method != null)
                {
                    this.DrawContextMethod(context, contextMemberInfo.Method);
                }
            }

            EditorGUI.indentLevel = prevIndentLevel;
        }

        private object DrawContextData(string memberName, Type memberType, object memberValue, int level)
        {
            if (level < MaxLevel)
            {
                var context = memberValue as Context;
                if (context != null)
                {
                    bool foldout;
                    this.foldoutDictionary.TryGetValue(level + memberName, out foldout);
                    foldout = this.foldoutDictionary[level + memberName] = EditorGUILayout.Foldout(foldout, memberName);
                    if (foldout)
                    {
                        this.DrawContextData(context, level + 1);
                    }

                    return context;
                }

                var dictionary = memberValue as DataDictionary;
                if (dictionary != null)
                {
                    bool foldout;
                    this.foldoutDictionary.TryGetValue(level + memberName, out foldout);
                    foldout = this.foldoutDictionary[level + memberName] = EditorGUILayout.Foldout(foldout, memberName);
                    if (foldout)
                    {
                        this.DrawContextData(dictionary, level + 1);
                    }

                    return dictionary;
                }

                var enumerable = memberValue as IEnumerable;
                if (enumerable != null && memberType.IsGenericType)
                {
                    var itemType = ReflectionUtils.GetEnumerableItemType(memberType);

                    bool foldout;
                    this.foldoutDictionary.TryGetValue(level + memberName, out foldout);
                    foldout = this.foldoutDictionary[level + memberName] = EditorGUILayout.Foldout(foldout, memberName);
                    if (foldout)
                    {
                        this.DrawContextData(enumerable, memberType, itemType, level + 1);
                    }

                    return enumerable;
                }
            }

            // Draw data trigger.
            var dataTrigger = memberValue as DataTrigger;
            if (dataTrigger != null)
            {
                InspectorUtils.DrawDataTrigger(memberName, dataTrigger);
                return dataTrigger;
            }

            return InspectorUtils.DrawValueField(memberName, memberType, memberValue,
                (name, type, value) => this.DrawCustomTypeData(name, type, value, level));
        }

        private void DrawContextData(IEnumerable enumerable, Type enumerableType, Type itemType, int level)
        {
            var isWriteable = enumerableType.GetInterfaces()
                .Any(x => x.IsGenericType &&
                          x.GetGenericTypeDefinition() == typeof(ICollection<>));

            var prevIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = level;

            var index = 0;
            List<object> itemsToRemove = null;
            foreach (var item in enumerable)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Item " + index);

                if (isWriteable && GUILayout.Button("Remove"))
                {
                    if (itemsToRemove == null)
                    {
                        itemsToRemove = new List<object>();
                    }

                    itemsToRemove.Add(item);
                }

                EditorGUILayout.EndHorizontal();
                this.DrawContextData("Item " + index, itemType, item, level);
                ++index;
            }

            if (itemsToRemove != null)
            {
                var removeMethod = enumerableType.GetMethods()
                    .FirstOrDefault(m => m.Name == "Remove" && m.GetParameters().Length == 1);
                if (removeMethod != null)
                {
                    foreach (var itemToRemove in itemsToRemove)
                    {
                        removeMethod.Invoke(enumerable, new[] {itemToRemove});
                    }
                }
            }

            if (isWriteable && GUILayout.Button("New Item"))
            {
                var addMethod = enumerableType.GetMethods()
                    .FirstOrDefault(m => m.Name == "Add" && m.GetParameters().Length == 1);
                if (addMethod != null)
                {
                    var newItem = Activator.CreateInstance(itemType);
                    addMethod.Invoke(enumerable, new[] {newItem});
                }
            }

            EditorGUI.indentLevel = prevIndentLevel;
        }

        private void DrawContextData(DataDictionary dataDictionary, int level)
        {
            var prevIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = level;

            Dictionary<object, object> changedValues = null;
            foreach (var key in dataDictionary.Keys)
            {
                var value = dataDictionary[key];
                var newValue = this.DrawContextData("Item " + key, dataDictionary.ValueType, value, level);
                if (!Equals(value, newValue))
                {
                    if (changedValues == null)
                    {
                        changedValues = new Dictionary<object, object>();
                    }

                    changedValues[key] = newValue;
                }
            }

            if (changedValues != null)
            {
                foreach (var key in changedValues.Keys)
                {
                    dataDictionary[key] = changedValues[key];
                }
            }

            GUILayout.BeginHorizontal();

            object newKey;
            this.newKeys.TryGetValue(dataDictionary, out newKey);
            var keyType = dataDictionary.KeyType;
            const string NewKeyLabel = "New:";
            if (keyType == typeof(string))
            {
                this.newKeys[dataDictionary] = newKey = EditorGUILayout.TextField(NewKeyLabel, (string) newKey);
            }
            else if (TypeInfoUtils.IsEnum(keyType))
            {
                if (newKey == null)
                {
                    newKey = Enum.GetValues(keyType).GetValue(0);
                }

                this.newKeys[dataDictionary] = newKey = EditorGUILayout.EnumPopup(NewKeyLabel, (Enum) newKey);
            }

            if (GUILayout.Button("+") && newKey != null)
            {
                var valueType = dataDictionary.ValueType;
                dataDictionary.Add(newKey, valueType.IsValueType ? Activator.CreateInstance(valueType) : null);
            }

            GUILayout.EndHorizontal();

            EditorGUI.indentLevel = prevIndentLevel;
        }

        private void DrawContextMethod(object context, MethodInfo method)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(ObjectNames.NicifyVariableName(method.Name));

            if (GUILayout.Button("Invoke"))
            {
                var parameterInfos = method.GetParameters();
                if (parameterInfos.Length > 0)
                {
                    var invokeEventPopup = new InvokeEventPopup(parameterInfos,
                        parameters => method.Invoke(context, parameters));
                    PopupWindow.Show(this.invokeEventPopupRect, invokeEventPopup);
                }
                else
                {
                    method.Invoke(context, null);
                }
            }

            if (Event.current.type == EventType.Repaint)
            {
                // Cover button with popup.
                var buttonRect = GUILayoutUtility.GetLastRect();
                this.invokeEventPopupRect = buttonRect;
                this.invokeEventPopupRect.position = new Vector2(buttonRect.position.x,
                    buttonRect.position.y - buttonRect.size.y);
            }

            EditorGUILayout.EndHorizontal();
        }

        private object DrawCustomTypeData(string memberName, Type memberType, object memberValue, int level)
        {
            bool foldout;
            this.foldoutDictionary.TryGetValue(level + memberName, out foldout);
            foldout = this.foldoutDictionary[level + memberName] = EditorGUILayout.Foldout(foldout, memberName);
            if (foldout)
            {
                this.DrawContextData(memberValue, level + 1);
            }

            return memberValue;
        }
    }
}